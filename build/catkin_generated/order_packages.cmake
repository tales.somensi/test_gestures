# generated from catkin/cmake/em/order_packages.cmake.em

set(CATKIN_ORDERED_PACKAGES "")
set(CATKIN_ORDERED_PACKAGE_PATHS "")
set(CATKIN_ORDERED_PACKAGES_IS_META "")
set(CATKIN_ORDERED_PACKAGES_BUILD_TYPE "")
list(APPEND CATKIN_ORDERED_PACKAGES "p3dx_controller")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "navigation/PioneerModel/p3dx_controller")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "openni_description")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "maestro/src/vision/ObjectDetectionAttention/src/openni_description")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "openni_launch")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "maestro/src/vision/ObjectDetectionAttention/src/openni_launch")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "p3dx_control")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "navigation/PioneerModel/p3dx_control")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "emotion")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "maestro/src/emotion")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "openni_camera")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "maestro/src/vision/ObjectDetectionAttention/src/openni_camera")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "rosaria")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "navigation/rosaria")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "p2os_urdf")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "lara_model/p2os_urdf")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "lara_model")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "lara_model/lara_model")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "p3dx_description")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "navigation/PioneerModel/p3dx_description")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "p3dx_gazebo")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "navigation/PioneerModel/p3dx_gazebo")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "navigation_gestures")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "navigation_gestures")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "p3dx_2dnav")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "navigation/p3dx_2dnav")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "wr_home_follow")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "maestro/src/follow")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")

set(CATKIN_MESSAGE_GENERATORS )

set(CATKIN_METAPACKAGE_CMAKE_TEMPLATE "/usr/lib/python3/dist-packages/catkin_pkg/templates/metapackage.cmake.in")
