#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/opt/athome_ws/devel:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/opt/athome_ws/devel/lib:$LD_LIBRARY_PATH"
export PKG_CONFIG_PATH="/opt/athome_ws/devel/lib/pkgconfig:$PKG_CONFIG_PATH"
export PWD='/opt/athome_ws/build'
export PYTHONPATH="/opt/athome_ws/devel/lib/python3/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES='/opt/athome_ws/devel/share/common-lisp'
export ROS_PACKAGE_PATH="/opt/athome_ws/src:$ROS_PACKAGE_PATH"