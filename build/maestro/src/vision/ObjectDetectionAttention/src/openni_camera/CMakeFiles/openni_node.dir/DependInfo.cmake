# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/opt/athome_ws/src/maestro/src/vision/ObjectDetectionAttention/src/openni_camera/src/nodes/openni_node.cpp" "/opt/athome_ws/build/maestro/src/vision/ObjectDetectionAttention/src/openni_camera/CMakeFiles/openni_node.dir/src/nodes/openni_node.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "BOOST_ALL_NO_LIB"
  "BOOST_ATOMIC_DYN_LINK"
  "BOOST_FILESYSTEM_DYN_LINK"
  "BOOST_SYSTEM_DYN_LINK"
  "BOOST_THREAD_DYN_LINK"
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"openni_camera\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/opt/athome_ws/src/maestro/src/vision/ObjectDetectionAttention/src/openni_camera/include"
  "/usr/include/ni"
  "/opt/ros/noetic/include"
  "/opt/ros/noetic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  "/opt/athome_ws/src/maestro/src/vision/ObjectDetectionAttention/src/openni_camera/BEFORE"
  "/opt/athome_ws/devel/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/opt/athome_ws/build/maestro/src/vision/ObjectDetectionAttention/src/openni_camera/CMakeFiles/openni_driver.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
