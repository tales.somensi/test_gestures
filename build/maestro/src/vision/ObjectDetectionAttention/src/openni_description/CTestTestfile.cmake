# CMake generated Testfile for 
# Source directory: /opt/athome_ws/src/maestro/src/vision/ObjectDetectionAttention/src/openni_description
# Build directory: /opt/athome_ws/build/maestro/src/vision/ObjectDetectionAttention/src/openni_description
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_openni_description_rostest_test_sample_kobuki.test "/opt/athome_ws/build/catkin_generated/env_cached.sh" "/usr/bin/python3" "/opt/ros/noetic/share/catkin/cmake/test/run_tests.py" "/opt/athome_ws/build/test_results/openni_description/rostest-test_sample_kobuki.xml" "--return-code" "/usr/bin/python3 /opt/ros/noetic/share/rostest/cmake/../../../bin/rostest --pkgdir=/opt/athome_ws/src/maestro/src/vision/ObjectDetectionAttention/src/openni_description --package=openni_description --results-filename test_sample_kobuki.xml --results-base-dir \"/opt/athome_ws/build/test_results\" /opt/athome_ws/src/maestro/src/vision/ObjectDetectionAttention/src/openni_description/test/sample_kobuki.test ")
set_tests_properties(_ctest_openni_description_rostest_test_sample_kobuki.test PROPERTIES  _BACKTRACE_TRIPLES "/opt/ros/noetic/share/catkin/cmake/test/tests.cmake;160;add_test;/opt/ros/noetic/share/rostest/cmake/rostest-extras.cmake;52;catkin_run_tests_target;/opt/athome_ws/src/maestro/src/vision/ObjectDetectionAttention/src/openni_description/CMakeLists.txt;13;add_rostest;/opt/athome_ws/src/maestro/src/vision/ObjectDetectionAttention/src/openni_description/CMakeLists.txt;0;")
