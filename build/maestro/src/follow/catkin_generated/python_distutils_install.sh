#!/bin/sh

if [ -n "$DESTDIR" ] ; then
    case $DESTDIR in
        /*) # ok
            ;;
        *)
            /bin/echo "DESTDIR argument must be absolute... "
            /bin/echo "otherwise python's distutils will bork things."
            exit 1
    esac
fi

echo_and_run() { echo "+ $@" ; "$@" ; }

echo_and_run cd "/opt/athome_ws/src/maestro/src/follow"

# ensure that Python install destination exists
echo_and_run mkdir -p "$DESTDIR/opt/athome_ws/install/lib/python3/dist-packages"

# Note that PYTHONPATH is pulled from the environment to support installing
# into one location when some dependencies were installed in another
# location, #123.
echo_and_run /usr/bin/env \
    PYTHONPATH="/opt/athome_ws/install/lib/python3/dist-packages:/opt/athome_ws/build/lib/python3/dist-packages:$PYTHONPATH" \
    CATKIN_BINARY_DIR="/opt/athome_ws/build" \
    "/usr/bin/python3" \
    "/opt/athome_ws/src/maestro/src/follow/setup.py" \
     \
    build --build-base "/opt/athome_ws/build/maestro/src/follow" \
    install \
    --root="${DESTDIR-/}" \
    --install-layout=deb --prefix="/opt/athome_ws/install" --install-scripts="/opt/athome_ws/install/bin"
