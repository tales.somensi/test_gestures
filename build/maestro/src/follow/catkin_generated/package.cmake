set(_CATKIN_CURRENT_PACKAGE "wr_home_follow")
set(wr_home_follow_VERSION "0.0.1")
set(wr_home_follow_MAINTAINER "Bruno Arantes <bruno.arantes@wr.sc.usp.br>")
set(wr_home_follow_PACKAGE_FORMAT "2")
set(wr_home_follow_BUILD_DEPENDS "rospy" "numpy" "std_msgs" "resettabletimer" "matplotlib")
set(wr_home_follow_BUILD_EXPORT_DEPENDS "rospy" "numpy" "std_msgs" "resettabletimer" "matplotlib")
set(wr_home_follow_BUILDTOOL_DEPENDS "catkin")
set(wr_home_follow_BUILDTOOL_EXPORT_DEPENDS )
set(wr_home_follow_EXEC_DEPENDS "rospy" "numpy" "std_msgs" "resettabletimer" "matplotlib")
set(wr_home_follow_RUN_DEPENDS "rospy" "numpy" "std_msgs" "resettabletimer" "matplotlib")
set(wr_home_follow_TEST_DEPENDS )
set(wr_home_follow_DOC_DEPENDS )
set(wr_home_follow_URL_WEBSITE "")
set(wr_home_follow_URL_BUGTRACKER "")
set(wr_home_follow_URL_REPOSITORY "")
set(wr_home_follow_DEPRECATED "")