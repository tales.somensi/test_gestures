# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "homer_tts: 7 messages, 0 services")

set(MSG_I_FLAGS "-Ihomer_tts:/opt/athome_ws/devel/share/homer_tts/msg;-Iactionlib_msgs:/opt/ros/noetic/share/actionlib_msgs/cmake/../msg;-Istd_msgs:/opt/ros/noetic/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(homer_tts_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakAction.msg" NAME_WE)
add_custom_target(_homer_tts_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "homer_tts" "/opt/athome_ws/devel/share/homer_tts/msg/SpeakAction.msg" "homer_tts/SpeakFeedback:homer_tts/SpeakActionResult:actionlib_msgs/GoalStatus:homer_tts/SpeakGoal:homer_tts/SpeakResult:homer_tts/SpeakActionFeedback:actionlib_msgs/GoalID:std_msgs/Header:homer_tts/SpeakActionGoal"
)

get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionGoal.msg" NAME_WE)
add_custom_target(_homer_tts_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "homer_tts" "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionGoal.msg" "homer_tts/SpeakGoal:actionlib_msgs/GoalID:std_msgs/Header"
)

get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionResult.msg" NAME_WE)
add_custom_target(_homer_tts_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "homer_tts" "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionResult.msg" "actionlib_msgs/GoalStatus:actionlib_msgs/GoalID:std_msgs/Header:homer_tts/SpeakResult"
)

get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionFeedback.msg" NAME_WE)
add_custom_target(_homer_tts_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "homer_tts" "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionFeedback.msg" "actionlib_msgs/GoalStatus:homer_tts/SpeakFeedback:actionlib_msgs/GoalID:std_msgs/Header"
)

get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakGoal.msg" NAME_WE)
add_custom_target(_homer_tts_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "homer_tts" "/opt/athome_ws/devel/share/homer_tts/msg/SpeakGoal.msg" ""
)

get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakResult.msg" NAME_WE)
add_custom_target(_homer_tts_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "homer_tts" "/opt/athome_ws/devel/share/homer_tts/msg/SpeakResult.msg" ""
)

get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakFeedback.msg" NAME_WE)
add_custom_target(_homer_tts_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "homer_tts" "/opt/athome_ws/devel/share/homer_tts/msg/SpeakFeedback.msg" ""
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(homer_tts
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakAction.msg"
  "${MSG_I_FLAGS}"
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakFeedback.msg;/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionResult.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/athome_ws/devel/share/homer_tts/msg/SpeakGoal.msg;/opt/athome_ws/devel/share/homer_tts/msg/SpeakResult.msg;/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionFeedback.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionGoal.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/homer_tts
)
_generate_msg_cpp(homer_tts
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakGoal.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/homer_tts
)
_generate_msg_cpp(homer_tts
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionResult.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/opt/athome_ws/devel/share/homer_tts/msg/SpeakResult.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/homer_tts
)
_generate_msg_cpp(homer_tts
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/athome_ws/devel/share/homer_tts/msg/SpeakFeedback.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/homer_tts
)
_generate_msg_cpp(homer_tts
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/homer_tts
)
_generate_msg_cpp(homer_tts
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/homer_tts
)
_generate_msg_cpp(homer_tts
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/homer_tts
)

### Generating Services

### Generating Module File
_generate_module_cpp(homer_tts
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/homer_tts
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(homer_tts_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(homer_tts_generate_messages homer_tts_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakAction.msg" NAME_WE)
add_dependencies(homer_tts_generate_messages_cpp _homer_tts_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionGoal.msg" NAME_WE)
add_dependencies(homer_tts_generate_messages_cpp _homer_tts_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionResult.msg" NAME_WE)
add_dependencies(homer_tts_generate_messages_cpp _homer_tts_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionFeedback.msg" NAME_WE)
add_dependencies(homer_tts_generate_messages_cpp _homer_tts_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakGoal.msg" NAME_WE)
add_dependencies(homer_tts_generate_messages_cpp _homer_tts_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakResult.msg" NAME_WE)
add_dependencies(homer_tts_generate_messages_cpp _homer_tts_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakFeedback.msg" NAME_WE)
add_dependencies(homer_tts_generate_messages_cpp _homer_tts_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(homer_tts_gencpp)
add_dependencies(homer_tts_gencpp homer_tts_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS homer_tts_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(homer_tts
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakAction.msg"
  "${MSG_I_FLAGS}"
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakFeedback.msg;/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionResult.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/athome_ws/devel/share/homer_tts/msg/SpeakGoal.msg;/opt/athome_ws/devel/share/homer_tts/msg/SpeakResult.msg;/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionFeedback.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionGoal.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/homer_tts
)
_generate_msg_eus(homer_tts
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakGoal.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/homer_tts
)
_generate_msg_eus(homer_tts
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionResult.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/opt/athome_ws/devel/share/homer_tts/msg/SpeakResult.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/homer_tts
)
_generate_msg_eus(homer_tts
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/athome_ws/devel/share/homer_tts/msg/SpeakFeedback.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/homer_tts
)
_generate_msg_eus(homer_tts
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/homer_tts
)
_generate_msg_eus(homer_tts
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/homer_tts
)
_generate_msg_eus(homer_tts
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/homer_tts
)

### Generating Services

### Generating Module File
_generate_module_eus(homer_tts
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/homer_tts
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(homer_tts_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(homer_tts_generate_messages homer_tts_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakAction.msg" NAME_WE)
add_dependencies(homer_tts_generate_messages_eus _homer_tts_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionGoal.msg" NAME_WE)
add_dependencies(homer_tts_generate_messages_eus _homer_tts_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionResult.msg" NAME_WE)
add_dependencies(homer_tts_generate_messages_eus _homer_tts_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionFeedback.msg" NAME_WE)
add_dependencies(homer_tts_generate_messages_eus _homer_tts_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakGoal.msg" NAME_WE)
add_dependencies(homer_tts_generate_messages_eus _homer_tts_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakResult.msg" NAME_WE)
add_dependencies(homer_tts_generate_messages_eus _homer_tts_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakFeedback.msg" NAME_WE)
add_dependencies(homer_tts_generate_messages_eus _homer_tts_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(homer_tts_geneus)
add_dependencies(homer_tts_geneus homer_tts_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS homer_tts_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(homer_tts
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakAction.msg"
  "${MSG_I_FLAGS}"
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakFeedback.msg;/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionResult.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/athome_ws/devel/share/homer_tts/msg/SpeakGoal.msg;/opt/athome_ws/devel/share/homer_tts/msg/SpeakResult.msg;/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionFeedback.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionGoal.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/homer_tts
)
_generate_msg_lisp(homer_tts
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakGoal.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/homer_tts
)
_generate_msg_lisp(homer_tts
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionResult.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/opt/athome_ws/devel/share/homer_tts/msg/SpeakResult.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/homer_tts
)
_generate_msg_lisp(homer_tts
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/athome_ws/devel/share/homer_tts/msg/SpeakFeedback.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/homer_tts
)
_generate_msg_lisp(homer_tts
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/homer_tts
)
_generate_msg_lisp(homer_tts
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/homer_tts
)
_generate_msg_lisp(homer_tts
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/homer_tts
)

### Generating Services

### Generating Module File
_generate_module_lisp(homer_tts
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/homer_tts
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(homer_tts_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(homer_tts_generate_messages homer_tts_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakAction.msg" NAME_WE)
add_dependencies(homer_tts_generate_messages_lisp _homer_tts_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionGoal.msg" NAME_WE)
add_dependencies(homer_tts_generate_messages_lisp _homer_tts_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionResult.msg" NAME_WE)
add_dependencies(homer_tts_generate_messages_lisp _homer_tts_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionFeedback.msg" NAME_WE)
add_dependencies(homer_tts_generate_messages_lisp _homer_tts_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakGoal.msg" NAME_WE)
add_dependencies(homer_tts_generate_messages_lisp _homer_tts_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakResult.msg" NAME_WE)
add_dependencies(homer_tts_generate_messages_lisp _homer_tts_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakFeedback.msg" NAME_WE)
add_dependencies(homer_tts_generate_messages_lisp _homer_tts_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(homer_tts_genlisp)
add_dependencies(homer_tts_genlisp homer_tts_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS homer_tts_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(homer_tts
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakAction.msg"
  "${MSG_I_FLAGS}"
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakFeedback.msg;/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionResult.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/athome_ws/devel/share/homer_tts/msg/SpeakGoal.msg;/opt/athome_ws/devel/share/homer_tts/msg/SpeakResult.msg;/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionFeedback.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionGoal.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/homer_tts
)
_generate_msg_nodejs(homer_tts
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakGoal.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/homer_tts
)
_generate_msg_nodejs(homer_tts
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionResult.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/opt/athome_ws/devel/share/homer_tts/msg/SpeakResult.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/homer_tts
)
_generate_msg_nodejs(homer_tts
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/athome_ws/devel/share/homer_tts/msg/SpeakFeedback.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/homer_tts
)
_generate_msg_nodejs(homer_tts
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/homer_tts
)
_generate_msg_nodejs(homer_tts
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/homer_tts
)
_generate_msg_nodejs(homer_tts
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/homer_tts
)

### Generating Services

### Generating Module File
_generate_module_nodejs(homer_tts
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/homer_tts
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(homer_tts_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(homer_tts_generate_messages homer_tts_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakAction.msg" NAME_WE)
add_dependencies(homer_tts_generate_messages_nodejs _homer_tts_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionGoal.msg" NAME_WE)
add_dependencies(homer_tts_generate_messages_nodejs _homer_tts_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionResult.msg" NAME_WE)
add_dependencies(homer_tts_generate_messages_nodejs _homer_tts_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionFeedback.msg" NAME_WE)
add_dependencies(homer_tts_generate_messages_nodejs _homer_tts_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakGoal.msg" NAME_WE)
add_dependencies(homer_tts_generate_messages_nodejs _homer_tts_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakResult.msg" NAME_WE)
add_dependencies(homer_tts_generate_messages_nodejs _homer_tts_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakFeedback.msg" NAME_WE)
add_dependencies(homer_tts_generate_messages_nodejs _homer_tts_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(homer_tts_gennodejs)
add_dependencies(homer_tts_gennodejs homer_tts_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS homer_tts_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(homer_tts
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakAction.msg"
  "${MSG_I_FLAGS}"
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakFeedback.msg;/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionResult.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/athome_ws/devel/share/homer_tts/msg/SpeakGoal.msg;/opt/athome_ws/devel/share/homer_tts/msg/SpeakResult.msg;/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionFeedback.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionGoal.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/homer_tts
)
_generate_msg_py(homer_tts
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakGoal.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/homer_tts
)
_generate_msg_py(homer_tts
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionResult.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/opt/athome_ws/devel/share/homer_tts/msg/SpeakResult.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/homer_tts
)
_generate_msg_py(homer_tts
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/athome_ws/devel/share/homer_tts/msg/SpeakFeedback.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/homer_tts
)
_generate_msg_py(homer_tts
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/homer_tts
)
_generate_msg_py(homer_tts
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/homer_tts
)
_generate_msg_py(homer_tts
  "/opt/athome_ws/devel/share/homer_tts/msg/SpeakFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/homer_tts
)

### Generating Services

### Generating Module File
_generate_module_py(homer_tts
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/homer_tts
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(homer_tts_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(homer_tts_generate_messages homer_tts_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakAction.msg" NAME_WE)
add_dependencies(homer_tts_generate_messages_py _homer_tts_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionGoal.msg" NAME_WE)
add_dependencies(homer_tts_generate_messages_py _homer_tts_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionResult.msg" NAME_WE)
add_dependencies(homer_tts_generate_messages_py _homer_tts_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakActionFeedback.msg" NAME_WE)
add_dependencies(homer_tts_generate_messages_py _homer_tts_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakGoal.msg" NAME_WE)
add_dependencies(homer_tts_generate_messages_py _homer_tts_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakResult.msg" NAME_WE)
add_dependencies(homer_tts_generate_messages_py _homer_tts_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/opt/athome_ws/devel/share/homer_tts/msg/SpeakFeedback.msg" NAME_WE)
add_dependencies(homer_tts_generate_messages_py _homer_tts_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(homer_tts_genpy)
add_dependencies(homer_tts_genpy homer_tts_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS homer_tts_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/homer_tts)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/homer_tts
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET actionlib_msgs_generate_messages_cpp)
  add_dependencies(homer_tts_generate_messages_cpp actionlib_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/homer_tts)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/homer_tts
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET actionlib_msgs_generate_messages_eus)
  add_dependencies(homer_tts_generate_messages_eus actionlib_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/homer_tts)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/homer_tts
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET actionlib_msgs_generate_messages_lisp)
  add_dependencies(homer_tts_generate_messages_lisp actionlib_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/homer_tts)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/homer_tts
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET actionlib_msgs_generate_messages_nodejs)
  add_dependencies(homer_tts_generate_messages_nodejs actionlib_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/homer_tts)
  install(CODE "execute_process(COMMAND \"/usr/bin/python3\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/homer_tts\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/homer_tts
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET actionlib_msgs_generate_messages_py)
  add_dependencies(homer_tts_generate_messages_py actionlib_msgs_generate_messages_py)
endif()
