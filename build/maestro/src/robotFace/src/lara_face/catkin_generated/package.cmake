set(_CATKIN_CURRENT_PACKAGE "lara_face")
set(lara_face_VERSION "0.0.0")
set(lara_face_MAINTAINER "antares <antares@todo.todo>")
set(lara_face_PACKAGE_FORMAT "2")
set(lara_face_BUILD_DEPENDS )
set(lara_face_BUILD_EXPORT_DEPENDS )
set(lara_face_BUILDTOOL_DEPENDS "catkin")
set(lara_face_BUILDTOOL_EXPORT_DEPENDS )
set(lara_face_EXEC_DEPENDS )
set(lara_face_RUN_DEPENDS )
set(lara_face_TEST_DEPENDS )
set(lara_face_DOC_DEPENDS )
set(lara_face_URL_WEBSITE "")
set(lara_face_URL_BUGTRACKER "")
set(lara_face_URL_REPOSITORY "")
set(lara_face_DEPRECATED "")