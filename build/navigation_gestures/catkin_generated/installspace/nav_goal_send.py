#!/usr/bin/env python3
# license removed for brevity

# source: https://hotblackrobotics.github.io/en/blog/2018/01/29/action-client-py/

import rospy
# Brings in the SimpleActionClient
import actionlib
# Brings in the .action file and messages used by the move base action
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
import time
import smach

class NavGoal(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success','fail'],
                            input_keys=['struct', 'location'],
                            output_keys=[])
        
        self._client = actionlib.SimpleActionClient('move_base',MoveBaseAction)

        self.locations ={   'corridor': [-5.5, 3.8],
                            'Close': [-0.55, 2.85],     # shelf
                            'Open': [3.0, 2.7],         # room
                            'Two': [-1.0, -2.0],        # table
                            'One': [3.1, -0.9]          # bench
                        }

    def log(self, str2print):
        output = "[" + time.strftime("%H:%M:%S", time.gmtime()) + "] -> " + str2print
        print(output)
        fh = open("log.txt", "a")
        fh.write(output+"\n\n")
        fh.close

    def execute(self, userdata):

        loc_x, loc_y = self.locations[userdata.location]

        # Create an action client called "move_base" with action definition file "MoveBaseAction"
        # client = actionlib.SimpleActionClient('move_base',MoveBaseAction)
    
        # Waits until the action server has started up and started listening for goals.
        # client.wait_for_server()
        self._client.wait_for_server()

        # Creates a new goal with the MoveBaseGoal constructor
        goal = MoveBaseGoal()
        goal.target_pose.header.frame_id = "map"
        goal.target_pose.header.stamp = rospy.Time.now()

        # Move 0.5 meters forward along the x axis of the "map" coordinate frame 
        goal.target_pose.pose.position.x = loc_x
        goal.target_pose.pose.position.y = loc_y

        # No rotation of the mobile base frame w.r.t. map frame
        goal.target_pose.pose.orientation.w = 1.0

        # Sends the goal to the action server.
        # client.execute(goal)
        self._client.send_goal(goal)
        # Waits for the server to finish performing the action.
        # wait = client.wait_for_result()
        wait = self._client.wait_for_result()
        # If the result doesn't arrive, assume the Server is not available
        if not wait:
            rospy.logerr("Action server not available!")
            rospy.signal_shutdown("Action server not available!")
        else:
            # Result of executing the action
            # return client.get_result()   
            nav_result = self._client.get_result()
            if nav_result:
                return 'success'
            return 'fail'

# If the python node is executed as main process (sourced directly)
if __name__ == '__main__':
    try:
       # Initializes a rospy node to let the SimpleActionClient publish and subscribe
        rospy.init_node('nav_goal_send')
        rngp = NavGoal()
        userdata = {"location": "bench"}
        result = rngp.execute(userdata)
        if result:
            rospy.loginfo("Goal execution done!")
    except rospy.ROSInterruptException:
        rospy.loginfo("Navigation test finished.")

# #!/usr/bin/env python3
# # license removed for brevity

# # source: https://hotblackrobotics.github.io/en/blog/2018/01/29/action-client-py/

# import rospy

# # Brings in the SimpleActionClient
# import actionlib
# # Brings in the .action file and messages used by the move base action
# from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal

# locations = {   'corridor': [-5.5, 3.8],
#                 'shelf': [-0.55, 2.85],
#                 'room': [3.0, 2.7],
#                 'table': [-1.0, -2.0],
#                 'bench': [3.1, -0.9]
#             }

# def send_goal(loc):

#     loc_x, loc_y = loc

#    # Create an action client called "move_base" with action definition file "MoveBaseAction"
#     client = actionlib.SimpleActionClient('move_base',MoveBaseAction)
 
#    # Waits until the action server has started up and started listening for goals.
#     client.wait_for_server()

#    # Creates a new goal with the MoveBaseGoal constructor
#     goal = MoveBaseGoal()
#     goal.target_pose.header.frame_id = "map"
#     goal.target_pose.header.stamp = rospy.Time.now()

#    # Move 0.5 meters forward along the x axis of the "map" coordinate frame 
#     goal.target_pose.pose.position.x = loc_x
#     goal.target_pose.pose.position.y = loc_y

#    # No rotation of the mobile base frame w.r.t. map frame
#     goal.target_pose.pose.orientation.w = 1.0

#    # Sends the goal to the action server.
#     client.send_goal(goal)
#    # Waits for the server to finish performing the action.
#     wait = client.wait_for_result()
#    # If the result doesn't arrive, assume the Server is not available
#     if not wait:
#         rospy.logerr("Action server not available!")
#         rospy.signal_shutdown("Action server not available!")
#     else:
#     # Result of executing the action
#         return client.get_result()   

# # If the python node is executed as main process (sourced directly)
# if __name__ == '__main__':
#     try:
#        # Initializes a rospy node to let the SimpleActionClient publish and subscribe
#         rospy.init_node('nav_goal_send')
#         result = send_goal(locations['shelf'])
#         if result:
#             rospy.loginfo("Goal execution done!")
#     except rospy.ROSInterruptException:
#         rospy.loginfo("Navigation test finished.")