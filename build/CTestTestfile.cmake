# CMake generated Testfile for 
# Source directory: /opt/athome_ws/src
# Build directory: /opt/athome_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("navigation/PioneerModel/p3dx_controller")
subdirs("maestro/src/vision/ObjectDetectionAttention/src/openni_description")
subdirs("maestro/src/vision/ObjectDetectionAttention/src/openni_launch")
subdirs("navigation/PioneerModel/p3dx_control")
subdirs("maestro/src/emotion")
subdirs("maestro/src/vision/ObjectDetectionAttention/src/openni_camera")
subdirs("navigation/rosaria")
subdirs("lara_model/p2os_urdf")
subdirs("lara_model/lara_model")
subdirs("navigation/PioneerModel/p3dx_description")
subdirs("navigation/PioneerModel/p3dx_gazebo")
subdirs("navigation_gestures")
subdirs("navigation/p3dx_2dnav")
subdirs("maestro/src/follow")
